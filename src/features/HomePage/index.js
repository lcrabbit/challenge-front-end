import React, { useState } from 'react';
import * as Styled from './styles';
import { Header, Footer, Book, Button, BookModal } from '@/components/index';

const HomePage = ({ initialBooks }) => {
  const [books, setBooks] = useState(initialBooks);
  const [openedBook, setOpenedBook] = useState(null);
  const [isModalOpen, setModalOpen] = useState(false);
  const [isLoadingData, setLoadingData] = useState(false);

  const fetchData = async () => {
    setLoadingData(true);
    try {
      const res = await fetch('https://www.googleapis.com/books/v1/volumes?q=HARRY%20POTTER');
      const json = await res.json();
      setBooks(json.items);
      setLoadingData(false);
    } catch (err) {
      setLoadingData(false);
      return { books: null };
    }
  }

  const openBook = (book) => {
    setOpenedBook(book);
    setModalOpen(true);
  }

  return (
    <React.Fragment>
      <Header />
      <Styled.Container>
        <Styled.Title>Books</Styled.Title>
        <Styled.Subtitle>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.
        </Styled.Subtitle>
      {!!books ?
      <Styled.BooksContainer>
        {books.map((book) => <Book book={book.volumeInfo} key={book.volumeInfo.title} onClick={(book) => openBook(book) } />)}
      </Styled.BooksContainer>
      :
      <React.Fragment>
        <Styled.Subtitle>Oops! We couldn't fetch the desired books data.</Styled.Subtitle>
        <Button isLoading={isLoadingData} onClick={() => fetchData()}>Try again</Button>
      </React.Fragment>
      }
      </Styled.Container>
      <BookModal ariaHideApp={false} onRequestClose={() => setModalOpen(false)} isOpen={isModalOpen} book={openedBook ?? null} />
      <Footer />
    </React.Fragment>
  );
};

export default HomePage;
