import styled from 'styled-components';
import { colors, breakpoint } from '@/utils/index';

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 82px 256px;
  background-color: ${colors.white};

  @media (max-width: ${breakpoint.medium}) {
    padding: 24px 48px;
  }
`;

export const Title = styled.h1`
  font-weight: 600;
  font-size: 24px;
  line-height: 32px;
  color: ${colors.lighterBlack};
`;

export const Subtitle = styled.p`
  font-size: 12px;
  line-height: 16px;
  margin-top: 24px;
  margin-bottom: 32px;
  text-align: center;
  max-width: 752px;
  color: ${colors.gray};
`;

export const BooksContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;

  > button {
    margin-right: 16px;
    margin-bottom: 16px;
  }
`;
