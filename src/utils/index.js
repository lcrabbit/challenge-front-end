export const colors = {
  primary: "#FCDB00",
  buttonText: "#333333",
  black: "#000000",
  lighterBlack: "#010101",
  white: "#FFFFFF",
  white2: "#F1F1F1",
  background: "#E5E5E5",
  gray: "#555555",
  lighterGray: "#777777"
};

export const breakpoint = {
  small: '420px',
  medium: '540px',
  hideIllustration: '1132px',
};
