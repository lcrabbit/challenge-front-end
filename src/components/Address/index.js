import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const Address = ({ addressLines }) => {
  return (
    <Styled.Content>
      {addressLines.map((address, index) => {
        return (
          <React.Fragment key={address + index}>
            <span>{address}</span>
            <br />
          </React.Fragment>
        );
      })}
    </Styled.Content>
  );
};

Address.propTypes = {
  addressLines: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Address;
