import styled from 'styled-components';
import { colors } from '@/utils/index';

export const Content = styled.p`
  font-size: 12px;
  color: ${colors.white};
`;
