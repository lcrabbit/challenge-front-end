import React from 'react';
import * as Styled from './styles';
import IconLink from '@/components/IconLink';

const Header = () => {
  return (
    <Styled.Container>
      <Styled.NavigationContainer>
        <Styled.Logo src="/svg/logo.svg" />
        <Styled.Navigation>
          <Styled.MenuLink href="#">Books</Styled.MenuLink>
          <Styled.MenuLink href="#">Newsletter</Styled.MenuLink>
          <Styled.MenuLink href="#">Address</Styled.MenuLink>
        </Styled.Navigation>
      </Styled.NavigationContainer>
      <Styled.HeadingContainer>
        <Styled.PresentationContainer>
          <Styled.Heading>Pixter Digital Books</Styled.Heading>
          <Styled.SubHeading>Lorem ipsum dolor sit amet? consectetur elit, volutpat.</Styled.SubHeading>
          <Styled.Description>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis. Proin viverra risus a eros volutpat tempor. In quis arcu et eros porta lobortis sit
          </Styled.Description>
          <Styled.PlatformsContainer>
            <IconLink href="#" icon="macos" />
            <IconLink href="#" icon="windows" />
            <IconLink href="#" icon="android" />
          </Styled.PlatformsContainer>
        </Styled.PresentationContainer>
        <Styled.Illustration src="/svg/illustration-tablet.svg" />
      </Styled.HeadingContainer>
      <Styled.CustomPagination amount={3} selected={0} />
    </Styled.Container>
  );
}

export default Header;
