import styled from 'styled-components';
import { colors, breakpoint } from '@/utils/index';
import Pagination from '@/components/Pagination';

export const Container = styled.header`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 88px 72px;
  background-color: ${colors.primary};

  @media (max-width: ${breakpoint.small}) {
    padding: 24px;
  }
`;

export const NavigationContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  align-items: center;
  justify-content: space-between;
`;

export const Logo = styled.img`
  margin-right: 24px;
`;

export const MenuLink = styled.a`
  margin-top: 2px;
  font-size: 24px;
  font-weight: 600;
  color: ${colors.lighterBlack};
  border-bottom: 2px solid transparent;
  text-decoration: none;
  transition: border-bottom 200ms ease;

  &:hover {
    border-bottom: 2px solid ${colors.lighterBlack};
  }
`;

export const Navigation = styled.nav`
  display: flex;
  justify-content: space-between;
  width: 376px;

  @media (max-width: ${breakpoint.small}) {
    margin-top: 24px;

    ${MenuLink} {
      font-size: 18px;
    }
  }

  @media (max-width: ${breakpoint.medium}) {
    ${MenuLink} {
      margin-right: 16px;

      &:last-child {
        margin-right: 0;
      }
    }
  }
`;

export const HeadingContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

export const PresentationContainer = styled.div`
  flex-grow: 1;
`;

export const Illustration = styled.img`
  width: 316px;
  height: 470px;

  @media (max-width: ${breakpoint.hideIllustration}) {
    display: none;
  }
`;

export const Heading = styled.h1`
  font-size: 32px;
  line-height: 42px;
  margin-top: 168px;
  font-weight: 600;
  color: ${colors.black};
`;

export const SubHeading = styled.h2`
  max-width: 304px;
  font-size: 16px;
  font-weight: normal;
  line-height: 24px;
  color: ${colors.black};
`;

export const Description = styled.h3`
  font-weight: 300;
  font-size: 16px;
  line-height: 24px;
  max-width: 672px;
`;

export const PlatformsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 162px;
  margin-top: 32px;
`;

export const PlatformIcon = styled.img`
  width: 36px;
  height: 36px;
`;

export const CustomPagination = styled(Pagination)`
  margin-top: 24px;
`;
