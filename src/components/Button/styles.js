import styled, { css } from 'styled-components';
import { colors } from '@/utils/index';

const sharedStyles = css`
  padding: 8px 32px;
  text-transform: uppercase;
  font-size: 12px;
  line-height: 16px;
  font-weight: 600;
  border: none;
  outline: none;
  border-radius: 8px;
  transition: opacity 200ms ease;
  color: ${colors.buttonText};
  background-color: ${colors.primary};

  &:hover {
    opacity: 0.85;
    cursor: ${props => props.isLoading ? 'wait' : 'pointer'};
  }

`;

export const Button = styled.button`
  ${sharedStyles};
`;

export const InputButton = styled.input`
  ${sharedStyles};
`;
