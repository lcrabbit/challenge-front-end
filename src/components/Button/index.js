import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const Button = ({ children, type, isLoading, ...props }) => {
  return (
    <React.Fragment>
      {type === 'button' ?
      <Styled.Button isLoading={isLoading} {...props}>{isLoading ? <img src="/svg/spinner.svg" /> : children}</Styled.Button>
      :
      <Styled.InputButton type="submit" value={children} {...props} />
      }
    </React.Fragment>
  )
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  isLoading: PropTypes.bool,
  type: PropTypes.oneOf(["button", "input"])
};

Button.defaultProps = {
  isLoading: false,
  type: 'button'
};

export default Button;
