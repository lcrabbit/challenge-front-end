import styled from 'styled-components';

export const Container = styled.button`
  outline: 0;
  border: 0;
  background: none;

  &:hover {
    cursor: pointer;
    -webkit-box-shadow: 2px 2px 6px 0px rgba(0,0,0,0.13);
    -moz-box-shadow: 2px 2px 6px 0px rgba(0,0,0,0.13);
    box-shadow: 2px 2px 6px 0px rgba(0,0,0,0.13);
  }
`;

export const Thumbnail = styled.img`
  object-fit: cover;
  width: 128px;
`;
