import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const Book = ({ book, onClick, ...props }) => {
  return (
    <Styled.Container onClick={() => onClick(book)} {...props}>
      <Styled.Thumbnail src={book && book.imageLinks && book.imageLinks.thumbnail || '/img/no-image.png'} />
    </Styled.Container>
  );
}

Book.propTypes = {
  book: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Book;
