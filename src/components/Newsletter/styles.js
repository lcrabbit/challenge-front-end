import styled from 'styled-components';
import { Input as BaseInput, Button as BaseButton } from '@/components/index';
import { breakpoint } from '@/utils/index';

export const Input = styled(BaseInput)``;

export const Button = styled(BaseButton)`
    width: 100%;
`;

export const Container = styled.form`
  display: flex;

  ${Input} {
    margin-right: 8px;
  }

  @media (max-width: ${breakpoint.medium}) {
    flex-direction: column;
    margin-right: 0;

    ${Input} {
      margin-right: 0;
      margin-bottom: 8px;
    }
  }
`;
