import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const Newsletter = ({ onSubmit, ...props }) => {
  const [value, setValue] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    if (value.trim() !== '') {
      onSubmit(value);
    } else {
      alert('An email is required to subscribe to our Newsletter!');
    }
  }

  return (
    <Styled.Container onSubmit={handleSubmit} {...props}>
      <Styled.Input
        value={value}
        placeholder="enter your email to update"
        onChange={(e) => setValue(e.target.value)}
      />
      <Styled.Button type="input">Submit</Styled.Button>
    </Styled.Container>
  );
};

Newsletter.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default Newsletter;
