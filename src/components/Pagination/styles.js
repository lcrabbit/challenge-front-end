import styled from 'styled-components';
import { colors } from '@/utils/index';

export const Paginator = styled.div`
  width: 24px;
  height: 24px;
  background-color: ${colors.white};
  border-radius: 100%;
  opacity: ${props => props.isSelected ? '1.0' : '0.7'};
`;

export const Container = styled.div`
  display: flex;

  ${Paginator} {
    margin-right: 16px;

    &:last-child {
      margin-right: 0;
    }
  }
`;
