import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const Pagination = ({ amount, selected, ...props }) => (
  <Styled.Container {...props}>
    {[...Array(amount)].map((_, index) => <Styled.Paginator key={index} isSelected={selected === index} />)}
  </Styled.Container>
);

Pagination.propTypes = {
  amount: PropTypes.number.isRequired,
  selected: PropTypes.number.isRequired,
};

export default Pagination;
