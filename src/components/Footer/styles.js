import styled from 'styled-components';
import { colors, breakpoint } from '@/utils/index';

export const Container = styled.footer`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 24px;
  background-color: ${colors.lighterBlack};

  @media (min-width: ${breakpoint.medium}) {
    padding: 82px;
  }
`;

export const Title = styled.span`
  font-weight: 600;
  font-size: 24px;
  line-height: 32px;
  color: ${colors.primary};
`;

export const About = styled.p`
  font-size: 12px;
  line-height: 16px;
  max-width: 752px;
  text-align: center;
  margin-bottom: 48px;
  color: ${colors.white2};
`;

export const SocialNetworks = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: 48px;

  > a {
    margin-right: 24px;

    &:last-child {
      margin-right: 0;
    }
  }
`;

export const AddressesContainer = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  margin-top: 48px;
  justify-content: space-between;

  > p {
    margin-right: 16px;
  }

  @media (max-width: ${breakpoint.medium}) {
    justify-content: center;
  }
`;

