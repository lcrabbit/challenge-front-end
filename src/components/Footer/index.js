import React from 'react';
import * as Styled from './styles';
import { Newsletter, IconLink, Address } from '@/components/index';

const addresses = [
  ['Alameda Santos, 1970', '6th floor - Jardim Paulista', 'São Paulo - SP', '+55 11 3090 8500'],
  ['London - UK', '125 Kingsway', 'London WC2B 6NH'],
  ['Lisbon - Portugal', 'Rua Rodrigues Faria, 103', '4th floor', 'Lisbon - Portugal'],
  ['Curitiba – PR', 'R. Francisco Rocha, 198', 'Batel – Curitiba – PR'],
  ['Buenos Aires – Argentina', 'Esmeralda 950', 'Buenos Aires B C1007']
];

const Footer = () => {
  const handleSubscribe = (email) => {
    console.log({
      email,
      site: 'https://google.com',
      accepted: true
    });
    alert('Subscription made with success!');
  }

  return (
    <Styled.Container>
      <Styled.Title>Keep in touch with us</Styled.Title>
      <Styled.About>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis venenatis.
      </Styled.About>
        <Newsletter onSubmit={handleSubscribe} />
      <Styled.SocialNetworks>
        <IconLink href="#" icon="facebook" size={48} />
        <IconLink href="#" icon="twitter" size={48} />
        <IconLink href="#" icon="google-plus" size={48} />
        <IconLink href="#" icon="pinterest" size={48} />
      </Styled.SocialNetworks>
        <Styled.AddressesContainer>
          {addresses.map((address) => <Address key={address} addressLines={address} />)}
        </Styled.AddressesContainer>
    </Styled.Container>
  );
};

export default Footer;
