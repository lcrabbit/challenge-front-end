import React from 'react';
import PropTypes from 'prop-types';
import * as Styled from './styles';

const IconLink = ({ icon, size, ...props }) => (
  <Styled.Link {...props}>
      <Styled.Icon src={`/svg/${icon}.svg`} size={size} />
  </Styled.Link>
);

IconLink.propTypes = {
  icon: PropTypes.string.isRequired,
  size: PropTypes.number
};

IconLink.defaultProps = {
  size: 32
};

export default IconLink;
