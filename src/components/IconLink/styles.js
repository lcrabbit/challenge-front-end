import styled from 'styled-components';

export const Icon = styled.img`
  width: ${props => props.size && props.size}px;
  height: ${props => props.size && props.size}px;
`;

export const Link = styled.a`
  text-decoration: none;
`;
