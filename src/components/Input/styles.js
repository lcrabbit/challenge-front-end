import styled from 'styled-components';
import { colors } from '@/utils/index';

export const Input = styled.input`
  line-height: 16px;
  padding: 8px 32px;
  border-radius: 8px;
  outline: none;
  border: none;
  border-radius: 8px;
  color: ${colors.gray};
  background: ${colors.white};

  &:focus {
    border: none;
    outline: none;
  }
`;
