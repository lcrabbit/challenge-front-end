import React from 'react';
import * as Styled from './styles';

const Input = ({...props}) => <Styled.Input {...props} />;

export default Input;
