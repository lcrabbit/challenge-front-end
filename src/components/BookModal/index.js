import React from 'react';
import Modal from 'react-modal';
import * as Styled from './styles';

const BookModal = ({ book, onRequestClose, ...props }) => {
  return (
    <Styled.CustomModal shouldCloseOnOverlayClick onRequestClose={onRequestClose} {...props}>
      <Styled.CloseButton onClick={onRequestClose}>
        <Styled.CloseIcon src="/svg/close.svg" />
      </Styled.CloseButton>
      <Styled.Title>{book && book.title}</Styled.Title>
      <Styled.Thumbnail src={book && book.imageLinks && book.imageLinks.thumbnail || '/img/no-image.png'} />
      <Styled.Published>{book && book.publishedDate ? `Published in ${book && book.publishedDate}` : 'No publication date available.'}</Styled.Published>
      <Styled.Detail><b>About:</b> {book && book.description || 'No description available.'}</Styled.Detail>
    </Styled.CustomModal>
  );
}

BookModal.setAppElement = Modal.setAppElement;
export default BookModal;
