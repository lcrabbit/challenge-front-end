import styled from 'styled-components';
import Modal from 'react-modal'
import { colors } from '@/utils/index';

const ReactModalAdapter = ({ className, modalClassName, ...props }) => {
  return (
    <Modal
      className={modalClassName}
      portalClassName={className}
      {...props}
    />
  )
};

export const CustomModal = styled(ReactModalAdapter).attrs({
  overlayClassName: 'Overlay',
  modalClassName: 'Modal'
})`
  .Overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, .8);
    z-index: 9999;
    display: flex;
    justify-content: center;
    align-items: center;

  }

  .Modal {
    align-items: center;
    max-width: 512px;
    max-height: calc(100vh - 48px);
    margin: 24px;
    position: relative;
    padding: 24px;
    background-color: ${colors.white};
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.08);
    text-align: center;
    user-select: none;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    border-radius: 8px;

    &:focus {
      outline: none;
    }
  }
`;

export const Title = styled.p`
  font-size: 16px;
  font-weight: 600;
  color: ${colors.primary};
`;

export const Thumbnail = styled.img`
  width: 128px;
`;

export const Published = styled.p`
  font-weight: 300;
  font-size: 12px;
  color: ${colors.lighterGray};
`;


export const Detail = styled.p`
  font-weight: 300;
  font-size: 14px;
  color: ${colors.lighterGray};
`;

export const CloseIcon = styled.img`
  width: 12px;
  height: 12px;
  margin-top: 2px;
`;

export const CloseButton = styled.button`
  position: absolute;
  right: 16px;
  top: 36px;
  height: 32px;
  width: 32px;
  outline: none;
  border: none;
  border-radius: 100%;
  transition: opacity 200ms ease;
  background: ${colors.primary};

  &:hover {
    cursor: pointer;
    opacity: 0.7;
  }
`;
