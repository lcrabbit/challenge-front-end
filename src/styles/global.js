import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    @font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 300;
    src: url('/fonts/open-sans-v17-latin-300.eot');
    src: local('Open Sans Light'), local('OpenSans-Light'),
        url('/fonts/open-sans-v17-latin-300.eot?#iefix') format('embedded-opentype'),
        url('/fonts/open-sans-v17-latin-300.woff2') format('woff2'),
        url('/fonts/open-sans-v17-latin-300.woff') format('woff'),
        url('/fonts/open-sans-v17-latin-300.ttf') format('truetype'),
        url('/fonts/open-sans-v17-latin-300.svg#OpenSans') format('svg');
    }

    @font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 400;
    src: url('/fonts/open-sans-v17-latin-regular.eot');
    src: local('Open Sans Regular'), local('OpenSans-Regular'),
        url('/fonts/open-sans-v17-latin-regular.eot?#iefix') format('embedded-opentype'),
        url('/fonts/open-sans-v17-latin-regular.woff2') format('woff2'),
        url('/fonts/open-sans-v17-latin-regular.woff') format('woff'),
        url('/fonts/open-sans-v17-latin-regular.ttf') format('truetype'),
        url('/fonts/open-sans-v17-latin-regular.svg#OpenSans') format('svg');
    }

    @font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 800;
    src: url('/fonts/open-sans-v17-latin-800.eot');
    src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'),
        url('/fonts/open-sans-v17-latin-800.eot?#iefix') format('embedded-opentype'),
        url('/fonts/open-sans-v17-latin-800.woff2') format('woff2'),
        url('/fonts/open-sans-v17-latin-800.woff') format('woff'),
        url('/fonts/open-sans-v17-latin-800.ttf') format('truetype'),
        url('/fonts/open-sans-v17-latin-800.svg#OpenSans') format('svg');
    }

  body {
    margin: 0;
    padding: 0;
    font-weight: 400;
    font-family: 'Open Sans';
  }
}`;
