import Head from 'next/head'
import HomePage from '@/features/HomePage';

export default function Home({ books }) {
  return (
    <React.Fragment>
      <Head>
        <title>Pixter Digital Books</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomePage initialBooks={books} />
    </React.Fragment>
  )
}

Home.getInitialProps = async (ctx) => {
  try {
    const res = await fetch('https://www.googleapis.com/books/v1/volumes?q=HARRY%20POTTER');
    const json = await res.json();
    return { books: json.items };
  } catch (err) {
    return { books: null };
  }
};
