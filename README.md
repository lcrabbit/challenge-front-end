This is a [Pixter](https://www.pixtertechnologies.com/) front-end challenge project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

```bash
yarn # to get dependencies installed
yarn dev # to run development server
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can also check a live demo [here](https://pixter-books-r1gqlgite.vercel.app/)

## FAQ
### Why styled components?
I've choosed the use of styled components because of it's organization and ease to use, separating each `styles.js` from it's respective `index.js`, which I think it is a nice architecture for newcomers in the code.

### React Modal
I thought it would be an overengineering to create a new abstraction, so for this case I decided to use `react-modal`.

### HTTP Client
As the project is simple, we can handle exceptions easily, and that's why I didn't used any abstraction for this. Instead, I handle exceptions by giving an interface to try to fetch the books data again. In a scenario where this project grows, I would choose [axios](https://github.com/axios/axios).

### Layout
I made minor changes by using (mostly) multiples of 8 to spacing and flexbox to avoid at most the needing of absolute positioning and to facilitate the responsiveness. But in general, I've followed the proposed layout.

### features/
This is a folder to cover components, but different from them, these components are equivalent to the contents of a page while those are minor UI components (such as a button, header, etc). As I chose to use Next.js, having this separation were mostly to follow the necessity in Next of each page in `pages/*` having an index file exporting the page component. With this architecture I can keep the `styles.js` separated without leaving too much information in only one file.
